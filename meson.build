project('gstasio', 'c', 'cpp',
  version : '0.1',
  meson_version : '>=0.45.1',
  default_options : [ 'buildtype=debugoptimized',
                      'warning_level=2'])

cxx = meson.get_compiler('cpp')

asiosdk_path = get_option('asiosdk-path')

if asiosdk_path == ''
  error('ASIO SDK Path is needed, pass with -Dasiosdk-path=C:/path/to/sdk')
endif

asiosdk_includes = [
  include_directories(join_paths(asiosdk_path, 'common'), is_system : true),
  include_directories(join_paths(asiosdk_path, 'host'), is_system : true),
  include_directories(join_paths(asiosdk_path, 'host', 'pc'), is_system : true),
]

if not cxx.has_header('asio.h', include_directories : asiosdk_includes)
  error('ASIO SDK not found: no asio.h')
endif

if not cxx.has_header('asiodrivers.h', include_directories : asiosdk_includes)
  error('ASIO SDK not found: no asiodrivers.h')
endif

asio_sources = files(
  join_paths(asiosdk_path, 'common', 'asio.cpp'),
  join_paths(asiosdk_path, 'host', 'asiodrivers.cpp'),
  join_paths(asiosdk_path, 'host', 'pc', 'asiolist.cpp')
)

asio_cpp_args = []
if cxx.get_id() != 'msvc'
  # Ignore any warnings emitted by the ASIO SDK code since we don't control it
  # Unfortunately we can't disable the -Wall passed by meson
  asio_cpp_args = ['-Wno-unused-but-set-variable']
  asio_sources += files('iasiothiscallresolver.cpp')
endif
libasiosdk = static_library('asiosdk', asio_sources,
  cpp_args : asio_cpp_args,
  include_directories : asiosdk_includes)

gst_dep = [
  dependency('gstreamer-1.0'),
  dependency('gstreamer-audio-1.0'),
]

gstasio_sources = files('gstasio.c',
                        'gstasiosrc.c',
                        'gstasioutil.cpp',
                        'gstasiosink.c')

shared_library('gstasio', gstasio_sources,
    link_with : libasiosdk,
    include_directories : asiosdk_includes,
    dependencies : gst_dep,
    c_args : ['-DPACKAGE="gstreamer"', '-DVERSION="@0@"'.format(meson.project_version())])
