/*
 * Copyright (C) 2018 Centricular Ltd.
 *   Author: Nirbheek Chauhan <nirbheek@centricular.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

/**
 * SECTION:element-asiosrc
 * @title: asiosrc
 *
 * Provides audio playback using the proprietary Steinberg ASIO API for Windows
 *
 * ## Example pipelines
 * |[
 * gst-launch-1.0 -v audiotestsrc samplesperbuffer=160 ! asiosrc
 * ]| Generate 20 ms buffers and render to the default audio device.
 *
 */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include "gstasiosrc.h"

// XXX: Change to src_debug later
GST_DEBUG_CATEGORY_STATIC (gst_asio_src_debug);
#define GST_CAT_DEFAULT gst_asio_src_debug

static GstStaticPadTemplate src_template = GST_STATIC_PAD_TEMPLATE ("src",
    GST_PAD_SRC,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS (GST_ASIO_STATIC_CAPS));

enum
{
  PROP_0,
  PROP_DEVICE,
  PROP_CAPTURE_CHANNELS,
};

static void gst_asio_src_dispose (GObject * object);
static void gst_asio_src_finalize (GObject * object);
static void gst_asio_src_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec);
static void gst_asio_src_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec);
static GstCaps *gst_asio_src_get_caps (GstBaseSrc * bsrc, GstCaps * filter);

static gboolean gst_asio_src_prepare (GstAudioSrc * asrc,
    GstAudioRingBufferSpec * spec);
static gboolean gst_asio_src_unprepare (GstAudioSrc * asrc);
static gboolean gst_asio_src_open (GstAudioSrc * asrc);
static gboolean gst_asio_src_close (GstAudioSrc * asrc);
static guint gst_asio_src_read (GstAudioSrc * asrc, gpointer data, guint length,
    GstClockTime * timestamp);
static guint gst_asio_src_delay (GstAudioSrc * asrc);
static void gst_asio_src_reset (GstAudioSrc * asrc);

#define gst_asio_src_parent_class parent_class
G_DEFINE_TYPE (GstAsioSrc, gst_asio_src, GST_TYPE_AUDIO_SRC);

static void
gst_asio_src_class_init (GstAsioSrcClass * klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
  GstElementClass *gstelement_class = GST_ELEMENT_CLASS (klass);
  GstBaseSrcClass *gstbasesrc_class = GST_BASE_SRC_CLASS (klass);
  GstAudioSrcClass *gstaudiosrc_class = GST_AUDIO_SRC_CLASS (klass);

  gobject_class->dispose = gst_asio_src_dispose;
  gobject_class->finalize = gst_asio_src_finalize;
  gobject_class->set_property = gst_asio_src_set_property;
  gobject_class->get_property = gst_asio_src_get_property;

  g_object_class_install_property (gobject_class,
      PROP_DEVICE,
      g_param_spec_string ("device", "Device",
          "ASIO device driver name to use", NULL,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS |
          GST_PARAM_MUTABLE_READY));

  g_object_class_install_property (gobject_class,
      PROP_CAPTURE_CHANNELS,
      g_param_spec_string ("capture-channels", "Capture Channels",
          "Comma-separated list of ASIO channels to capture", NULL,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS |
          GST_PARAM_MUTABLE_READY));

  gst_element_class_add_static_pad_template (gstelement_class, &src_template);
  gst_element_class_set_static_metadata (gstelement_class, "AsioSrc",
      "Src/Audio",
      "Stream audio to an audio device through ASIO",
      "Nirbheek Chauhan <nirbheek@centricular.com>");

  gstbasesrc_class->get_caps = GST_DEBUG_FUNCPTR (gst_asio_src_get_caps);

  gstaudiosrc_class->prepare = GST_DEBUG_FUNCPTR (gst_asio_src_prepare);
  gstaudiosrc_class->unprepare = GST_DEBUG_FUNCPTR (gst_asio_src_unprepare);
  gstaudiosrc_class->open = GST_DEBUG_FUNCPTR (gst_asio_src_open);
  gstaudiosrc_class->close = GST_DEBUG_FUNCPTR (gst_asio_src_close);
  gstaudiosrc_class->read = GST_DEBUG_FUNCPTR (gst_asio_src_read);
  gstaudiosrc_class->delay = GST_DEBUG_FUNCPTR (gst_asio_src_delay);
  gstaudiosrc_class->reset = GST_DEBUG_FUNCPTR (gst_asio_src_reset);

  GST_DEBUG_CATEGORY_INIT (gst_asio_src_debug, "asiosrc", 0,
      "Steinberg ASIO src");
}

static void
gst_asio_src_init (GstAsioSrc * self)
{
  self->drivers_allocated = FALSE;
  self->driver_loaded = FALSE;
  self->driver_name = NULL;
  self->channels = NULL;

  if (gst_asio_util_allocate_drivers (GST_ELEMENT (self)))
    self->drivers_allocated = TRUE;
}

static void
gst_asio_src_dispose (GObject * object)
{
  GstAsioSrc *self = GST_ASIO_SRC (object);

  g_array_free (self->channels, TRUE);
  self->channels = NULL;

  G_OBJECT_CLASS (gst_asio_src_parent_class)->dispose (object);
}

static void
gst_asio_src_finalize (GObject * object)
{
  GstAsioSrc *self = GST_ASIO_SRC (object);

  g_clear_pointer (&self->driver_name, g_free);

  if (self->cached_caps != NULL) {
    gst_caps_unref (self->cached_caps);
    self->cached_caps = NULL;
  }

  gst_asio_util_deallocate_drivers (GST_ELEMENT (self));

  G_OBJECT_CLASS (gst_asio_src_parent_class)->finalize (object);
}

static void
gst_asio_src_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec)
{
  GstAsioSrc *self = GST_ASIO_SRC (object);

  switch (prop_id) {
    case PROP_DEVICE:
      g_free (self->driver_name);
      self->driver_name = g_value_dup_string (value);
      break;
    case PROP_CAPTURE_CHANNELS:
    {
      gchar **channels;
      guint ii, channel, n_channels;

      channels = g_strsplit (g_value_get_string (value), ",", 0);
      n_channels = g_strv_length (channels);
      self->channels = g_array_new (FALSE, FALSE, sizeof (guint));
      for (ii = 0; ii < n_channels; ii++) {
        GST_INFO ("Selecting channel '%s'", channels[ii]);
        /* Ignore empty strings to handle repeated and trailing commas */
        if (!channels[ii])
          continue;
        channel = g_ascii_strtoull (channels[ii], NULL, 0);
        g_array_append_val (self->channels, channel);
      }
      g_strfreev (channels);
      break;
    }
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static void
gst_asio_src_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec)
{
  GstAsioSrc *self = GST_ASIO_SRC (object);

  switch (prop_id) {
    case PROP_DEVICE:
      g_value_set_string (value, self->driver_name);
      break;
    case PROP_CAPTURE_CHANNELS:
    {
      guint ii, channel;
      GString *channels = g_string_new (NULL);
      for (ii = 0; ii < self->channels->len; ii++) {
        channel = g_array_index (self->channels, guint, ii);
        g_string_append_printf (channels, "%u,", channel);
      }
      g_value_take_string (value, channels->str);
      g_string_free (channels, FALSE);
      break;
    }
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static GstCaps *
gst_asio_src_get_caps (GstBaseSrc * bsrc, GstCaps * filter)
{
  GstAsioSrc *self = GST_ASIO_SRC (bsrc);
  GstCaps *caps = NULL;

  GST_DEBUG_OBJECT (self, "entering get caps");

  if (self->cached_caps) {
    caps = gst_caps_ref (self->cached_caps);
  } else {
    GstCaps *template_caps;
    guint max_channels = 0;
    gboolean ret;

    template_caps = gst_pad_get_pad_template_caps (bsrc->srcpad);

    if (!self->driver_loaded) {
      caps = template_caps;
      goto out;
    }

    if (self->channels)
      max_channels = self->channels->len;

    ret = gst_asio_util_get_device_format (GST_ELEMENT (self), TRUE,
        template_caps, max_channels, &caps);
    if (!ret) {
      GST_ELEMENT_ERROR (self, STREAM, FORMAT, (NULL),
          ("failed to detect device format"));
      gst_caps_unref (template_caps);
      return NULL;
    }

    gst_caps_replace (&self->cached_caps, caps);
    gst_caps_unref (template_caps);
  }

  if (filter) {
    GstCaps *filtered =
        gst_caps_intersect_full (filter, caps, GST_CAPS_INTERSECT_FIRST);
    gst_caps_unref (caps);
    caps = filtered;
  }


out:
  GST_DEBUG_OBJECT (self, "returning caps %" GST_PTR_FORMAT, caps);
  return caps;
}

static gboolean
gst_asio_src_open (GstAudioSrc * asrc)
{
  GstAsioSrc *self = GST_ASIO_SRC (asrc);

  if (self->driver_name)
    GST_DEBUG_OBJECT (self, "loading driver %s", self->driver_name);
  else
    GST_DEBUG_OBJECT (self, "loading the first driver found");

  if (self->driver_loaded)
    return TRUE;

  if (!gst_asio_util_initialize (GST_ELEMENT (self), self->driver_name)) {
      GST_ELEMENT_ERROR (self, RESOURCE, OPEN_WRITE, (NULL),
          ("Failed to load driver %s", self->driver_name));
    return FALSE;
  }

  self->driver_loaded = TRUE;

  return TRUE;
}

static gboolean
gst_asio_src_close (GstAudioSrc * asrc)
{
  GstAsioSrc *self = GST_ASIO_SRC (asrc);

  gst_asio_util_deinitialize (GST_ELEMENT (self));

  return TRUE;
}

static gboolean
gst_asio_src_prepare (GstAudioSrc * asrc, GstAudioRingBufferSpec * spec)
{
  GstAsioSrc *self = GST_ASIO_SRC (asrc);
  glong buffer_size;
  gboolean res = FALSE;

  if (!gst_asio_util_create_buffers (GST_ELEMENT (self), spec, self->channels,
      TRUE, &buffer_size))
    goto beach;

  /* Set our ringbuffer's segsize as ASIO's buffer_size (for all channels
   * combined), and segtotal as 2 for the lowest possible latency. This will be
   * automatically translated to latency_time and buffer_time. */
  spec->segsize = buffer_size;
  spec->segtotal = 2;

  if (!gst_asio_util_start (GST_ELEMENT (self)))
    goto beach;

  res = TRUE;
beach:
  if (!res)
    gst_asio_src_unprepare (asrc);

  return res;
}

static gboolean
gst_asio_src_unprepare (GstAudioSrc * asrc)
{
  GstAsioSrc *self = GST_ASIO_SRC (asrc);

  gst_asio_util_dispose_buffers (GST_ELEMENT (self));

  return TRUE;
}

static guint
gst_asio_src_read (GstAudioSrc * asrc, gpointer data, guint length,
    GstClockTime * timestamp G_GNUC_UNUSED)
{
  GstAsioSrc *self = GST_ASIO_SRC (asrc);

  return gst_asio_util_src_read (GST_ELEMENT (self), data, length);
}

static guint
gst_asio_src_delay (GstAudioSrc * asrc G_GNUC_UNUSED)
{
  //GstAsioSrc *self = GST_ASIO_SRC (asrc);

  /* TODO: Implement this with ASIOGetLatencies */
  return 0;
}

static void
gst_asio_src_reset (GstAudioSrc * asrc)
{
  GstAsioSrc *self = GST_ASIO_SRC (asrc);

  gst_asio_util_stop (GST_ELEMENT (self));
  /* TODO: restart on next call to src_read */
}
