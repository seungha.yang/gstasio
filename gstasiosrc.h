/*
 * Copyright (C) 2018 Centricular Ltd.
 *   Author: Nirbheek Chauhan <nirbheek@centricular.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef __GST_ASIO_SRC_H__
#define __GST_ASIO_SRC_H__

#include "gstasioutil.h"

G_BEGIN_DECLS
#define GST_TYPE_ASIO_SRC \
  (gst_asio_src_get_type ())
#define GST_ASIO_SRC(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GST_TYPE_ASIO_SRC, GstAsioSrc))
#define GST_ASIO_SRC_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), GST_TYPE_ASIO_SRC, GstAsioSrcClass))
#define GST_IS_ASIO_SRC(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GST_TYPE_ASIO_SRC))
#define GST_IS_ASIO_SRC_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), GST_TYPE_ASIO_SRC))
typedef struct _GstAsioSrc GstAsioSrc;
typedef struct _GstAsioSrcClass GstAsioSrcClass;

struct _GstAsioSrc
{
  GstAudioSrc parent;
  /* whether the drivers have been queried and allocated */
  gboolean drivers_allocated;
  /* whether the specified driver has been loaded */
  gboolean driver_loaded;

  /* caps returned by the driver */
  GstCaps *cached_caps;

  /* properties */

  /* the driver to be loaded */
  gchar *driver_name;

  /* channels to capture */
  GArray *channels;
};

struct _GstAsioSrcClass
{
  GstAudioSrcClass parent_class;
};

GType gst_asio_src_get_type (void);

G_END_DECLS
#endif /* __GST_ASIO_SRC_H__ */
